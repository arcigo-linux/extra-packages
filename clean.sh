#!/usr/bin/env bash

if [ -d packages ]; then
    rm -r packages/ 2> /dev/null
    echo -e "\n==> Packages cleaned !"
else
    echo -e "\n==> Packages directory not found !"
    exit 1
fi

